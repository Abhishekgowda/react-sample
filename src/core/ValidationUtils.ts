export class ValidationUtils {
  public isMobileNumberValid(ph: string) {
    const regX = new RegExp(/^[0-9]{10,10}$/);
    return regX.test(ph);
  }

  public isMobileNumberLengthValid(phoneNumber: string) {
    return phoneNumber.length === 10;
  }

  public isEmailValid(email: string) {
    const regX = new RegExp(/^[a-zA-Z0-9_]+(\.[_a-zA-Z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/);
    return regX.test(email);
  }

  public isEmpty(str: string) {
    return str.length === 0 ? true : false;
  }

  public isPasswordValid(password: string) {
    const regX = new RegExp(/^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&?@."]).*$/);
    return regX.test(password);
  }
}
