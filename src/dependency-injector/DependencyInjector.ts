import { BaseState } from "../view-model/BaseState";
import { UserRepository } from "../domain/repository/userRepository";
import { ValidationUtils } from "../core/ValidationUtils";
import { isNullOrUndefined } from "util";
import { LoginViewModel } from "../view-model/LoginViewModel";
import { ApiManager } from "../api/ApiManager";

export class DependencyInjector {
  private static dependencyInjector: DependencyInjector;
  private baseState: BaseState;
  private userRepository: UserRepository;
  private ValidationUtils: ValidationUtils;
  private apiManager: ApiManager = {};

  constructor() {
    if (isNullOrUndefined(this.apiManager)) {
      this.apiManager = new ApiManager();
    }
    this.userRepository = new UserRepository(this.apiManager);
    this.baseState = new BaseState(this.userRepository);
    this.ValidationUtils = new ValidationUtils();
  }

  provideBaseState(): BaseState {
    return this.baseState;
  }

  public static initialize(): void {
    if (!DependencyInjector.dependencyInjector) {
      DependencyInjector.dependencyInjector = new DependencyInjector();
    }
  }

  public static default(): DependencyInjector {
    if (isNullOrUndefined(DependencyInjector.dependencyInjector)) {
      DependencyInjector.dependencyInjector = new DependencyInjector();
    }
    return this.dependencyInjector;
  }

  provideLoginViewModel(): LoginViewModel {
    return new LoginViewModel(this.baseState, this.userRepository, this.ValidationUtils);
  }
}
