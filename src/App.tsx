import React from "react";
import "./App.css";
import { ComponentBase } from "resub";
import Login from "./View/Login/Login";
import { Home } from "./View/Home/Home";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import { BrowserRouter as Router, Switch, Route, Redirect, RouteComponentProps } from "react-router-dom";
import { DependencyInjector } from "./dependency-injector/DependencyInjector";
import { IBaseState, BaseState } from "./view-model/BaseState";

class App extends ComponentBase<any, IBaseState> {
  baseState: BaseState;
  constructor(props: any) {
    super(props);
    DependencyInjector.initialize();
    this.baseState = DependencyInjector.default().provideBaseState();
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route
            path="/Login"
            render={(props: RouteComponentProps) => <Login baseState={this.state} routerProps={props} />}
          />
          <ProtectedRoute
            path="/Home"
            render={(props: any) => {
              console.log("propsprops", props);
              return <Home baseState={this.state} routerProps={props} />;
            }}
            isLoggedIn={this.state.isLoggedIn}
          />
          <Redirect to="/Login" from="/" />
        </Switch>
      </Router>
    );
  }
}

export default App;
