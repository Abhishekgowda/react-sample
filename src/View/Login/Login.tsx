import React from "react";
import { ComponentBase } from "resub";
import { ILoginState, LoginViewModel, ILoginProps } from "../../view-model/LoginViewModel";
import { DependencyInjector } from "../../dependency-injector/DependencyInjector";

export default class Login extends ComponentBase<any, any> {
  private viewModel: LoginViewModel;
  constructor(props: any) {
    super(props);
    this.viewModel = DependencyInjector.default().provideLoginViewModel();
  }

  loginClicked = () => {
    console.log("login clicked", this.props);
    this.viewModel.loginClicked();
  };

  render() {
    return (
      <form>
        <div>
          <label>Username : </label> <br />
          <input type="text" placeholder="Enter Username" name="username" required /> <br />
          <label>Password : </label> <br />
          <input type="password" placeholder="Enter Password" name="password" required /> <br />
          <button type="submit" onClick={this.loginClicked}>
            Login
          </button>
          <br />
          <input type="checkbox" /> <label>Remember me?</label> <br />
          Forgot <a href="#"> password? </a>
        </div>
      </form>
    );
  }
}
