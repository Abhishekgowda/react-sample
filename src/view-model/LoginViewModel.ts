import { BaseViewModel } from "./BaseViewModel";
import { IBaseState, BaseState } from "./BaseState";
import { UserRepository } from "../domain/repository/userRepository";
import { ValidationUtils } from "../core/ValidationUtils";

export interface ILoginState {
  isLoginDialogOpen: boolean;
  mobileNumber: string;
  isOtpGenerated: boolean;
  otp: string;
  isLoading: boolean;
  mobileNumberError?: Error;
  otpError?: Error;
  generateOtpResponse: any;
  error?: Error;
  verifyOtpResponse: any;
}

export interface ILoginProps {
  isOpen: boolean;
  push(screnName: string): any;
  close(): any;
}

export class LoginViewModel extends BaseViewModel {
  protected state: ILoginState;
  // protected userRepository: UserRepository;
  constructor(baseState: BaseState, userRepository: UserRepository, validationUtils: ValidationUtils) {
    super(userRepository);
    // this.userRepository = userRepository;
    this.state = this.defaultState();
  }

  defaultState(): ILoginState {
    return {
      isLoginDialogOpen: false,
      mobileNumber: "",
      isOtpGenerated: false,
      otp: "",
      isLoading: false,
      mobileNumberError: undefined,
      otpError: undefined,
      generateOtpResponse: undefined,
      error: undefined,
      verifyOtpResponse: undefined,
    };
  }

  async loginClicked(): Promise<boolean> {
    // const value = await this.userRepository.loginClicked();
    this.setState({
      ...this.state,
      isLoggedIn: true,
    });
    console.log("+++++++++", this.state);
    return Promise.resolve(true);
  }
}
